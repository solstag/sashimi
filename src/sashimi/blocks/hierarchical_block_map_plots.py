import colorcet
import pandas
from bokeh import (
    plotting as bkp,
    models as bkm,
    events as bke,
    io as bio,
)
from bokeh.models.callbacks import CustomJS
from bokeh.models.formatters import DatetimeTickFormatter

from .annotations import (
    load_annotations,
    get_xblock_yblocks_elements,
    get_subxblocks_yblocks_elements,
)
from .util import (
    sorted_hierarchical_block_index,
    try_time_index,
    is_time_type,
)


def get_doc_histogram_data(corpus, series: pandas.Series = None):
    series = corpus.data[corpus.col_time] if series is None else series
    series, xindex = try_time_index(series)
    blocks, levels = corpus.get_blocks_levels("doc")
    choice_vals = []
    for map_level in list(reversed(levels)):
        map_hbindex = sorted_hierarchical_block_index(blocks, levels, map_level)
        for map_hb in map_hbindex:
            bdata = corpus.data[blocks[map_level] == map_hb[-1]]
            bseries = series[blocks[map_level] == map_hb[-1]]
            val = bdata.groupby(bseries).size().reindex(xindex).fillna(0)
            choice_vals.append(val)
    choice_vals[0].index.name = series.name
    return choice_vals


def get_doc_histogram(btype, choice_vals, tap_fig):
    xaxis = choice_vals[0].index
    is_time = is_time_type(xaxis)
    source = bkm.ColumnDataSource(
        {
            "x": xaxis,
            "y": [0] * len(xaxis),
            "yrel": [0] * len(xaxis),
            "ytot": choice_vals[0],
        }
    )

    fig = bkp.figure(
        toolbar_location=None,
        tools="",
        min_height=150,
        sizing_mode="stretch_both",
        x_axis_type="datetime" if is_time else "linear",
        # x_axis_label=f"{xaxis.name}",
        y_axis_label="documents",
    )

    if is_time:
        fig.xaxis.formatter = DatetimeTickFormatter(days="%d %b")

    rel_rend = fig.line(
        "x", "yrel", source=source, color="red", y_range_name="rel", legend_label="rel"
    )
    fig.line("x", "y", source=source, color="grey", legend_label="abs")

    fig.y_range.start = 0.0
    fig.extra_y_ranges = {"rel": bkm.DataRange1d(renderers=[rel_rend], start=0.0)}
    fig.add_layout(
        bkm.LinearAxis(y_range_name="rel", major_label_text_color="red"), "right"
    )
    fig.legend.location = "top_left"
    fig.x_range.bounds = "auto"
    dochist_cb = CustomJS(
        args=dict(
            fig=fig,
            source=source,
            choice_vals=choice_vals,
        ),
        code="""
        const index = (
            cb_obj.indices === undefined || cb_obj.indices[0] === undefined ?
            0 : cb_obj.indices[0]
        )
        const data = {...source.data}
        data.y = choice_vals[index]
        data.yrel = new Float64Array(data.y).map(
            (val, i) => val ? val / data.ytot[i] : val
        )
        source.data = data
        fig.x_range.reset()
        fig.y_range.reset()
        fig.extra_y_ranges["rel"].reset()
        """,
    )
    bio.curdoc().js_on_event("document_ready", dochist_cb)
    tap_source = tap_fig.select_one(f"{btype}_map_data")
    tap_source.selected.js_on_change("change:indices", dochist_cb)

    wheelzoom_tool = bkm.WheelZoomTool(
        dimensions="width",
        maintain_focus=False,
        zoom_on_axis=True,
    )
    pan_tool = bkm.PanTool(dimensions="width")
    hover_tool = bkm.HoverTool(
        tooltips=[("", ("@x{%F}" if is_time else "@x") + " (@y / @ytot)")],
        formatters={"@x": "datetime"} if is_time else {},
        mode="vline",
        attachment="right",
        renderers=[rel_rend],
    )
    fig.add_tools(wheelzoom_tool, pan_tool, hover_tool)
    fig.toolbar.active_scroll = wheelzoom_tool
    fig.toolbar.active_drag = pan_tool

    return fig


def get_element_histogram_data(corpus, btype, series: pandas.Series = None):
    series = corpus.data[corpus.col_time] if series is None else series
    series, xindex = try_time_index(series)
    blocks, levels = corpus.get_blocks_levels(btype)
    x_documents = corpus.ter_documents if btype == "ter" else corpus.ext_documents
    choice_vals = []
    for map_level in list(reversed(levels)):
        map_hbindex = sorted_hierarchical_block_index(blocks, levels, map_level)
        for map_hb in map_hbindex:
            bdocs = {
                doc
                for el in blocks[blocks[map_level].eq(map_hb[-1])].index
                for doc in x_documents[el]
            }
            bdatasel = corpus.data.index.isin(bdocs)
            bdocdata = corpus.data[bdatasel]
            bseries = series[bdatasel]
            val = bdocdata.groupby(bseries).size().reindex(xindex).fillna(0)
            choice_vals.append(val)
    choice_vals[0].index.name = series.name
    return choice_vals


def get_multi_histogram_data(corpus, ybtype, series: pandas.Series = None):
    annotations_spe = load_annotations(
        corpus, get_xblock_yblocks_elements, "doc", ybtype
    )
    annotations_com = load_annotations(
        corpus, get_subxblocks_yblocks_elements, "doc", ybtype
    )

    lblock_to_label = corpus.lblock_to_label
    yblocks, ylevels = corpus.get_blocks_levels(ybtype)
    y_element_to_block = yblocks[1]
    if ybtype == "ext":
        doc_exts = corpus.get_doc_exts()

    def counting_f(domain_data, level, xblock):
        annotations = annotations_spe if level == 1 else annotations_com

        xblock_yblocks = annotations[level][xblock]["blocks"]
        # number of terms in domain_data belonging to each yblock
        yblocks_count = {lblock_to_label[1, yb]: 0 for yb in xblock_yblocks}
        if ybtype == "ter":
            for doc in domain_data[corpus.column]:
                for yb in {
                    yb
                    for sen in doc
                    for ter in sen
                    if (yb := y_element_to_block[ter]) in xblock_yblocks
                }:
                    yblocks_count[lblock_to_label[1, yb]] += 1
        else:
            for doc_idx in domain_data.index:
                for yb in {
                    yb
                    for ext in doc_exts[doc_idx]
                    if (yb := y_element_to_block[ext]) in xblock_yblocks
                }:
                    yblocks_count[lblock_to_label[1, yb]] += 1
        yblocks_count = pandas.Series(yblocks_count, dtype="float64").fillna(0)
        return yblocks_count

    series = corpus.data[corpus.col_time] if series is None else series
    series, xindex = try_time_index(series)

    dblocks, dlevels = corpus.get_blocks_levels("doc")
    choice_vals = []
    for level in list(reversed(dlevels)):
        hbindex = sorted_hierarchical_block_index(dblocks, dlevels, level)
        for hb in hbindex:
            domain_data = corpus.data[dblocks[level] == hb[-1]]
            bseries = series[dblocks[level] == hb[-1]]
            df_sums = domain_data.groupby(bseries).apply(counting_f, level, hb[-1])
            val = df_sums.reindex(xindex).fillna(0)
            choice_vals.append(val)
    choice_vals[0].index.name = xindex.name
    return choice_vals


def get_multi_histogram(dfs, tap_fig, ylabel):
    xaxis = dfs[0].index
    is_time = is_time_type(xaxis)
    fig = bkp.figure(
        toolbar_location=None,
        tools="",
        sizing_mode="stretch_both",
        x_axis_type="datetime" if is_time else "linear",
        x_axis_label=f"{xaxis.name}",
        y_axis_label=f"{ylabel} presence",
    )

    if is_time:
        fig.xaxis.formatter = DatetimeTickFormatter(days="%d %b")
    fig.y_range.start = 0.0
    tap_source = tap_fig.select_one("doc_map_data")
    multihist_cb = CustomJS(
        args=dict(
            tap_source=tap_source.selected,
            xaxis=xaxis.to_list(),
            choice_yaxis=[df.to_dict(orient="list") for df in dfs],
            fig=fig,
            colors=colorcet.glasbey_dark[1:],
        ),
        code="""
        const ColumnDataSource = Bokeh.Models.get("ColumnDataSource")
        const Line = Bokeh.Models.get("Line")
        const VAreaStep = Bokeh.Models.get("VAreaStep")
        const index = (
            tap_source.indices === undefined
            ? undefined
            : tap_source.indices[0]
        )
        fig.x_range.bounds = "auto"
        fig.renderers = []
        const block2yaxis = index === undefined ? {} : choice_yaxis[index]
        let num = 0
        for (const yname in block2yaxis) {
            const y_values = block2yaxis[yname]
            const source = new ColumnDataSource({data: {x: xaxis, y: y_values}})
            const line = new Line({
                x: { field: "x" },
                y: { field: "y" },
                line_color: colors[num],
            });
            fig.add_glyph(line, source)
            num += 1
            if (num === 5) { break }
        }
        if (num == 0) {
            const source = new ColumnDataSource(
                {data: {x: xaxis, y: xaxis.map(x => 0)}}
            )
            const line = new Line({x: { field: "x" }, y: { field: "y" }});
            fig.add_glyph(line, source)
            fig.renderers[0].visible = false
        }
        fig.x_range.reset()
        fig.y_range.reset()
        """,
    )
    bio.curdoc().js_on_event("document_ready", multihist_cb)
    tap_source.selected.js_on_change("change:indices", multihist_cb)

    relative_mode = bkm.Select(value="absolute", options=["absolute", "relative"])
    fig.js_on_event(
        bke.Tap,
        CustomJS(
            args=dict(relative_mode=relative_mode, reload_data=multihist_cb),
            code="""
        const renderers = cb_obj.origin.renderers
        if (!renderers[0].data_source.data.hasOwnProperty("y")) return
        if (relative_mode.value == "absolute") {
            let total = renderers[0].data_source.data.y.map(x => 0)
            for (const rend of renderers) {
                const data = rend.data_source.data
                total = total.map((x, i) => x + data.y[i])
            }
            for (const rend of renderers) {
                const data = {...rend.data_source.data}
                data.y = data.y.map((x, i) => x ? x / total[i] : 0)
                rend.data_source.data = data
            }
            relative_mode.value = "relative"
        } else {
            reload_data.execute()
            relative_mode.value = "absolute"
        }
    """,
        ),
    )

    wheelzoom_tool = bkm.WheelZoomTool(
        dimensions="width",
        maintain_focus=False,
        zoom_on_axis=True,
    )
    pan_tool = bkm.PanTool(dimensions="width")
    hover_tool = bkm.HoverTool(
        tooltips=[("", ("@x{%F}" if is_time else "@x") + " (@y)")],
        formatters={"@x": "datetime"} if is_time else {},
        mode="mouse",
        attachment="left",
    )
    fig.add_tools(wheelzoom_tool, pan_tool, hover_tool)
    fig.toolbar.active_scroll = wheelzoom_tool
    fig.toolbar.active_drag = pan_tool

    return fig
