"""
col = "week" ;
area_rank_chart(c, slice(None), f"_{col}", f"_{col}", 1, outfile=f"arc-{col}.html") ;
"""

import math

from numpy import linspace, cos, pi
import colorcet
import bokeh.models as bkm
import bokeh.events as bke
from bokeh.io import output_file, show
from bokeh.layouts import row, column
from bokeh.models import ColumnDataSource, CustomJS
from bokeh.plotting import figure
import pandas as pd

from .tables import get_subxblocks, get_labels


###################
# Area rank chart #
###################


def area_rank_chart(
    corpus, selector, grouper, averager, level, labels=None, rel=True, outfile=None
):
    """
    Plots an area rank chart of domains, with the grouper dimension on the
    x-axis and domain sizes on the y-axis.

    Restricts corpus to given selector, then group by grouper and by blocks of
    the given level. Get the group's sizes, then average on averager. For
    example, if grouper corresponds to periods with variable number of years,
    then averager should be the year, so that larger periods aren't inflated.
    """
    # Data
    grouper_multiplicity = corpus.data.groupby(grouper)[averager].unique().map(len)
    selected_data = corpus.data.loc[selector]
    y_range = selected_data.groupby(corpus.dblocks[level]).size().index
    y_counts = (
        selected_data.groupby([grouper, corpus.dblocks[level]], sort=False)
        .size()
        .div(grouper_multiplicity, level=0)
    )
    y_abs = [
        y_counts.loc[(idx,)]
        .reindex(y_range, fill_value=0, level=1)
        .sort_values(ascending=False, kind="stable")
        for idx in y_counts.index.levels[0]
    ]
    y_abs_max = max(yi.max() for yi in y_abs)
    if rel:
        y_rel = [x / x.sum() for x in y_abs]
        y_rel_max = max(yi.max() for yi in y_rel)
    points_in_unit_width = max(13, 900 // len(y_abs))

    # Labels
    def get_block_label(b):
        label = corpus.lblock_to_label[level, b]
        if label in label_to_user_label:
            return f"{label}: {label_to_user_label[label]}"
        else:
            return label

    label_to_user_label = get_labels(corpus) if labels is None else {}
    idx_to_full_label = {idx: get_block_label(idx) for idx in y_range}

    # Figure
    fig = figure(
        sizing_mode="stretch_both",
        x_range=bkm.Range1d(
            0, len(y_abs) - 1, bounds="auto", max_interval=len(y_abs), min_interval=1
        ),
        x_axis_location="above",
        y_range=bkm.Range1d(
            len(y_range), 0, bounds="auto", max_interval=len(y_range), min_interval=1
        ),
        toolbar_location="above",
        tools="pan, box_zoom, save, reset",
    )
    fig.axis.major_label_text_font_size = "17px"
    fig.xaxis.minor_tick_line_color = None
    fig.xaxis.major_label_orientation = math.pi / 4
    fig.xaxis.ticker = list(range(len(y_counts.index.levels[0])))
    fig.xaxis.major_label_overrides = {
        i: str(j) for i, j in enumerate(y_counts.index.levels[0])
    }
    fig.ygrid.visible = False
    fig.yaxis.ticker = yticker = [i + 1 / 2 for i in range(len(y_abs[0]))]
    fig.yaxis.major_label_overrides = {
        i: str(j) for i, j in zip(yticker, y_abs[0].index.map(idx_to_full_label.get))
    }

    # Add end ticks and labels
    ryaxis = bkm.LinearAxis(ticker=bkm.FixedTicker())
    fig.add_layout(ryaxis, "right")
    ryaxis.major_label_text_font_size = "17px"
    ryaxis.ticker = ryticker = [i + 1 / 2 for i in range(len(y_abs[-1]))]
    ryaxis.major_label_overrides = {
        i: str(j) for i, j in zip(ryticker, y_abs[-1].index.map(idx_to_full_label.get))
    }

    if rel:
        renderers_rel = plot_connections(
            fig, y_range, y_rel, y_rel_max, points_in_unit_width
        )
    renderers_abs = plot_connections(
        fig, y_range, y_abs, y_abs_max, points_in_unit_width
    )
    hover_renderers = renderers_rel if rel else renderers_abs

    # Tools
    fig.add_tools(
        bkm.HoverTool(
            tooltips=[("", "@idx{custom}")],
            renderers=hover_renderers,
            formatters={
                "@idx": bkm.CustomJSHover(
                    args=dict(idx_to_label=idx_to_full_label),
                    code="""return idx_to_label.get(value)""",
                )
            },
        )
    )

    wheelzoom_tool = bkm.WheelZoomTool(maintain_focus=False)
    fig.add_tools(wheelzoom_tool)
    fig.toolbar.active_scroll = wheelzoom_tool

    fig.js_on_event(
        bke.RangesUpdate,
        CustomJS(
            args=dict(lyaxis=fig.yaxis, ryaxis=ryaxis),
            code="""
                const [start, end] = this.origin.x_range.computed_bounds
                this.origin.left[0].visible = Math.abs(start - this.x0) < 1/2
                this.origin.right[0].visible = Math.abs(end - this.x1) < 1/2
            """,
        ),
    )

    # Output
    if outfile is None:
        return fig
    output_file(outfile, title="Area rank chart")
    show(fig)


def plot_connections(fig, y_range, y_data, y_max_data, points_in_unit_width):
    def append_to_source(idx, xs, y0s, y1s):
        if idx not in sources:
            sources[idx] = ColumnDataSource(data=dict(xs=[], y0s=[], y1s=[], idx=[]))
        source = sources[idx]
        source.data["xs"].extend(xs)
        source.data["y0s"].extend(y0s)
        source.data["y1s"].extend(y1s)
        source.data["idx"].extend([idx for _ in xs])

    sources = {}
    palette = colorcet.glasbey_dark
    hover_color = palette[0]
    indexed_palette = pd.Series(palette[1 : len(y_range) + 1], index=y_range)

    pairs_of_x_values = zip(y_data, y_data[1:])
    for start, (y_current, y_next) in enumerate(pairs_of_x_values):
        all_data_indexes = y_current.index.append(y_range.difference(y_current.index))
        for i, idx in enumerate(all_data_indexes):
            y0pos = i + 1 / 2
            size0 = y_current.loc[idx]
            y1pos = (len(y_next.loc[:idx]) - 1) + 1 / 2
            size1 = y_next.loc[idx]
            xs, y0s, y1s = connection(
                y0=y0pos,
                y1=y1pos,
                s0=size0 / y_max_data,
                s1=size1 / y_max_data,
                start=start,
                length=1,
                points_in_unit_width=points_in_unit_width,
            )
            append_to_source(idx, xs, y0s, y1s)

    renderers = []
    for source in sources.values():
        plt = fig.varea(
            "xs",
            "y0s",
            "y1s",
            color=indexed_palette[source.data["idx"][0]],
            alpha=0.5,
            hover_color=hover_color,
            hover_alpha=1,
            source=source,
        )
        renderers.append(plt)

    return renderers


def connection(y0, y1, s0, s1, start, length, points_in_unit_width):
    def connect(x):  # from 0 to 1
        return y0 + (y1 - y0) * (1 - cos(x * pi)) / 2

    def spread(x):
        return s0 * (1 - x) + s1 * x

    xs = linspace(start, start + length, int(abs(length) * points_in_unit_width))
    nxs = linspace(0, 1, int(abs(length) * points_in_unit_width))
    y0s = [connect(x) - spread(x) / 2 for x in nxs]
    y1s = [connect(x) + spread(x) / 2 for x in nxs]
    return xs, y0s, y1s


########################
# Stacked domain plots #
########################


def domain_stacked_charts(corpus, domain_labels, outfile="evoplot.html"):
    fig = build_stacked_charts(
        corpus,
        [
            corpus.level_block_to_hblock(level, block, btype)
            for dlabel in domain_labels
            for btype, level, block in [corpus.label_to_tlblock(dlabel)]
        ],
    )
    if outfile is None:
        return fig
    output_file(outfile)
    show(fig)


def subdomain_stacked_charts(corpus, xlevel, xb, sxlevel=None, outfile="evoplot.html"):
    layout = column(
        build_stacked_charts(corpus, [corpus.level_block_to_hblock(xlevel, xb, "doc")]),
        build_stacked_charts(
            corpus, get_subxblocks(corpus, "doc", xlevel, xb, sxlevel=sxlevel)
        ),
        # rank_graphics(),
    )
    if outfile is None:
        return layout
    output_file(outfile)
    show(layout)


def build_stacked_charts(corpus, dhblocks):
    """
    Stacked charts showing the comparative evolution of domains.
    """
    x = sorted(corpus.data[corpus.col_time].unique())

    def get_sizes(level_block):
        level, block = level_block
        return (
            corpus.data[corpus.dblocks[level] == block]
            .groupby(corpus.col_time)
            .size()
            .reindex(x)
            .fillna(0)
        )

    dlevels = [corpus.hblock_to_level_block(dhb, "doc")[0] for dhb in dhblocks]
    labels = get_labels(corpus)

    cats_data = list(reversed(list(zip(dhblocks, dlevels))))
    cats_data = sorted(
        cats_data, key=lambda x: get_sizes((x[1], x[0][-1])).sum(), reverse=True
    )
    cats = [corpus.lblock_to_label[level, dhb[-1]] for dhb, level in cats_data]
    cats = [f"{x}: {labels.get(x, '[no label]')}" for x in cats]

    out_size = corpus.data.groupby(corpus.col_time).size().reindex(x)
    in_size = sum(get_sizes((level, dhb[-1])) for (dhb, level) in cats_data).astype(int)
    has_in_size = not out_size.equals(in_size)

    palette = colorcet.glasbey_dark[1 : 1 + len(cats_data)]

    def make_plot(norm, scale, title, labels):
        p = figure(
            title=title,
            y_range=cats + [""],
            width=1920 // (3 if has_in_size else 2),
            height=1080,
            x_range=(min(x), max(x)),
        )

        def ridge(category, data):
            return [(category, dat * scale * len(dhblocks) / 2) for dat in data]

        for i, (cat, (dhb, level)) in enumerate(
            zip(reversed(cats), reversed(cats_data))
        ):
            source = ColumnDataSource(data=dict(x=x))
            y_counts = (
                corpus.data[corpus.dblocks[level] == dhb[-1]]
                .groupby(corpus.col_time)
                .size()
                .reindex(x)
                .fillna(0)
            )
            y_values = y_counts / norm
            y1 = len(y_values) * [(cat, 0.0)]
            y2 = ridge(cat, y_values)
            source.add(y2, cat)
            source.add(y1, f"{cat}_zero")
            source.add(y_counts, f"{cat}_counts")
            source.add(norm, "norm")

            p.varea(
                x="x",
                y1=f"{cat}_zero",
                y2=cat,
                color=palette[i],
                alpha=0.5,
                source=source,
            )
            line = p.line(
                x="x",
                y=cat,
                color="black",
                source=source,
            )

            tip = f"@{{{cat}_counts}}" + ("" if title == "Absolute" else " / @{norm}")
            p.add_tools(
                bkm.HoverTool(
                    renderers=[line],
                    tooltips=[("", tip)],
                    mode="vline",
                )
            )

        p.outline_line_color = None
        p.background_fill_color = "#efefef"
        p.ygrid.grid_line_color = None
        p.xgrid.grid_line_color = "#dddddd"
        p.xgrid.ticker = p.xaxis.ticker
        p.axis.minor_tick_line_color = None
        p.axis.major_tick_line_color = None
        p.axis.axis_line_color = None
        if not labels:
            p.yaxis.major_label_text_font_size = "0px"

        return p

    layout = row(
        make_plot(pd.Series(1, index=x), 1 / in_size.mean(), "Absolute", True),
        make_plot(out_size, out_size.sum() / in_size.sum(), "Corpus relative", False),
        align="end",
    )
    if has_in_size:
        layout.append(make_plot(in_size, 1, "Ensemble relative", False))

    return layout
