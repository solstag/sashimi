<h1>Essential Sashimi etiquette</h1>
<p>
  The idea behind a domain-topic model is to group documents that tend to share the same terms, and group terms that tend to appear in the same documents. Each group of documents will thus contain documents that share a preference for certain groups of terms, just as each group of terms will contain terms that are preferentially employed across certain groups of documents.
</p>

<h2>The domain-topic model glossary</h2>
<p>
  <strong>Block</strong>: a group of things, one thing can only belong to a single block.
</p>
<p>
  <strong>Domain</strong>: a block (group) of documents; or, at higher levels, a block of domains from the level below.
</p>
<p>
  <strong>Topic</strong>: a block (group) of elements, usually a document's terms; or, at higher levels, a block of topics from the level below.
</p>
<p>
  <strong>Level-1 blocks</strong>: simple blocks, containing elements such as documents, terms, dates, names etc.
</p>
<p>
  <strong>Level-N blocks (N>1)</strong>: super-blocks, containing blocks of the level below it, called its sub-blocks.
</p>
<p>
  At the highest level, all blocks coalesce into a single super-block. For domains this is the full corpus. For topics, this is the corpus' vocabulary.
</p>
<p>
  <strong>Chained block</strong>: a block of any dimension of the corpus, obtained not from symmetric relationships like domains and topics, but conditioned to a previous partition of the corpus into domains, and thus reflecting the semantics of this partition. For example, document's authors can be grouped in respect to the domains they publish in, while those domains were first obtained in respect to the terms employed in their documents.
</p>

<h2>A la carte</h2>
<p>
  The map shown here are hierarchical block maps. Each column stacks the blocks of a given level, in such a way that moving horizontally from a block leads to the blocks' super-block to the left, and to its sub-blocks to the right.
</p>
<p>
  To help distinguish the different types of blocks, domains are consistently presented in tones of salmon, while topics employ the color tuna. Wasabi is used for chained blocks.
</p>
<p>Features of blocks on the map:
  <ol>
    <li>Bottom-right: the block's label; indicates the block's level, type, and number (e.g. L2D13, L1T3, L2E3).
    <li>Bottom-middle as well as the block's height:
      <ul>
        <li>For domains: the number of documents it contains.
        <li>For other types of blocks: the total usage of their elements (e.g., for a topic, the sum of the frequencies of its terms).
      </ul>
    <li>Bottom-left: numeric values for the salience of each block; the values will depend on the current selection of blocks.
    <li>Color intensity: conveys values for the salience of each block; the values will depend on the current selection of blocks.
      <ul>
        <li>To adapt to the different scales presented in the block hierarchy, values are normalized at each level, so that the strongest color matches the strongest value for that level.
        <li>As a consequence, color comparisons are only valid within each column of the same map. Use the numeric values for cross-level comparisons.
      </ul>
    <li>From top-left and downwards:
      <ul>
        <li>For domains:
          <ul>
            <li>At level = 1: <em>specific elements</em> — topics or chained blocks that are most specific to the domain in question, followed by their elements most specific.
            <li>At level > 1: <em>common elements</em> — topics or chained blocks that are specific to all subdomains of the domain, followed by elements alike. It may happen that no element has such a property.
            <li>These lists are preceded by a number that tells the strength of the specificity/commonality, so that when it is <em>0.0</em> there's nothing to display.
          </ul>
        <li>For topics and chained blocks:
          <ul>
            <li><em>frequent elements</em>: top elements from the block, ordered by highest frequency in the corpus.
          </ul>
      </ul>
  </ol>
</p>
<p>
In the starting maps, salience values correspond to the quantities shown in the bottom-middle divided by their totals: fractions of all documents found in a domain; fraction of total term usage coming from a topic. As we'll see, clicking on blocks on the opposite map will change this, as explained below.
</p>

<h2>Map interactivity, <i>maihashi</i></h2>
<p>
  <strong>Scroll</strong> down and up to zoom in and out of blocks. To help navigate the hierarchy, zooming in on a block stops when the block fills the frame. Move the cursor lower in the hierarchy (to the right) to continue zooming. Zooming on a block that is partially hidden immediately brings it entirely into the frame.
</p>
<p>
  When zooming, <strong>drag</strong> the map up or down to move through adjacent blocks.
</p>
<p>
  <strong>Click</strong> on a block to select and learn more about it: on the opposite side, the "map" and "info" tabs will be updated with information on the selected block. <em>Shift-click</em> selects multiple blocks, <em>Ctrl-Shift-click</em> on a selected block deselects it, and <em>Ctrl-click</em> outside of selections deselects all. Pressing <em>Escape</em> on the keyboard deselect everything in all maps.
</p>

<h3>Upon selecting a domain</h3>
<p>
  The <strong>color</strong> and <strong>values</strong> of the opposite map (topic or chained) will be updated to show, for each topic or chained block, the logarithm of the ratio between how much the topic is used in the selected domain(s) against how much it gets used overall. This quantity represents how much a topic is over (positive) or under (negative) represented in the selected domain, with shades of grey standing for negative values.
</p>
<h4>The <em>Domain info</em> tab</h4>
<p>
  Two line plots may appear on top of the tab. The first represents the distribution of documents in the corpus along some dimension, usually time. The red line tells the fraction of total documents at that point, and the grey line the absolute number of documents. When a domain gets selected, the distribution is updated to count only documents contained in the selected domain. Under multiple selection, only the results for the first selection are shown. The second plot shows multiple colored lines, one for each characteristic topic of the chosen domain, representing the distribution, along the abscissa, of documents using that topic. Clicking anywhere on the second plot makes it switch between absolute and relative values.
</p>
<p>
  The tab also displays the full information found on top of the domain block(s) on the map, followed by a list of <em>titles</em> from documents in the domain, with links to those documents' URLs if those were provided when building the map.
</p>
<h4>Multiple selection</h4>
<p>When selecting multiple domains, the opposite (topic or chained) map's color and value will depend on the choice presented in the selector to the right of the Search bar:
  <ul>
    <li>Single: keeps those of the first chosen domain.
    <li>Multi AND: for each topic, picks the minimum among the chosen domains.
    <li>Multi OR: for each topic, picks the maximum among the chosen domains.
  </ul>
</p>

<h3>Upon selecting a topic or chained block</h3>
<p>
  The <strong>color</strong> and <strong>values</strong> of the Domain map will be updated to show, for each domain, the logarithm of the ratio between how much the selected block is used in each domain against how much it gets used overall. This quantity represents how much a domain over (positive) or under (negative) represents the selected block, with shades of grey standing for negative values.
</p>
<h4>The <em>Topic</em> or <em>Chained</em> <em>info</em> tab</h4>
<p>
  A line plot may appear on top of the tab. At first, it represents the distribution of documents in the corpus along some dimension, usually time. The red line tells the fraction of total documents at that point, and the grey line the absolute number of documents. When a topic or chained block gets selected, the distribution is updated to count only documents containing any element from the selected block — its terms or chained elements. Under multiple selection, only the results for the first selection are shown.
</p>
<p>
<h4>Multiple selection</h4>
<p>When selecting multiple blocks, the Domain map's color and value is transformed in a manner analogous to selecting multiple domains.</p>
<p>
  The <em>info</em> tab, in addition to the full information seen on top of the block(s), will provide a list of <em>titles</em> from documents that contain any elements from the block. At level 1, <em>frequent elements</em> displays the full list of elements in the block.
</p>

<h3>Searching</h3>

Above the maps, you'll find a <em>Search bar</em> that lets you select blocks by searching for associated labels (i.e. "L2D3", "L4T2"), elements (i.e. terms like "body" or "soul") or document titles. Use it wisely.

<h2>Au revoir, <em>またいつか</em></h2>
<h3>Bibliography (also to credit where appropriate)</h3>
<p>
  Hannud Abdo, A., Cointet, J.-P., Bourret, P., & Cambrosio, A. (2022). Domain-topic models with chained dimensions: Charting an emergent domain of a major oncology conference. Journal of the Association for Information Science and Technology, 73( 7), 992– 1011. <a href="https://doi.org/10.1002/asi.24606">https://doi.org/10.1002/asi.24606</a>
</p>
