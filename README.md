# Sashimi — study the organization and evolution of corpora

A Python module for mixed-methods qualitative studies of large textual or
symbolic corpora, providing applications of statistical models accompanied by
tailored interactive visualizations. It affords the detection of both textual
and metadata structures by employing stochastic block modeling (SBM) from
`graph-tool` or (currently deprecated) word embedding from `gensim`.

### Statistical models
- Domain-topic models
  - Synthesizes document clustering (domain modeling) and topic modeling (term clustering).
- Domain-chained models
  - Extrapolates modeled domains to partition other dimensions of the corpus such as time, people, institutions, categories or places.

### Human model-data interfaces
- Domain maps
  - An interactive HTML document to explore the corpus and its clusters, enhanced with histograms and a search function.
- Domain tables
  - Ideal for systematic annotation of clusters.
- Domain networks
  - To visualize relations between domains, topics and clusters of other dimensions.
- Area rank charts
  - Beautiful "bump charts" displaying the evolution of domains.

<img alt="Domain-topic network" src="https://docs.cortext.net/wp-content/uploads/Screenshot-from-2022-11-19-17-42-28.png" width="50%"><img alt="Domain-topic map" src="https://docs.cortext.net/wp-content/uploads/Image-1.png" width="50%">

## Eat in: "no code" usage

Much of the functionality of `sashimi` is available as a suite of methods in the [Cortext
Manager](https://docs.cortext.net/) web service. See their documentation for
[SASHIMI](https://docs.cortext.net/sashimi/), which may also serve users of the Python module as an introduction to the methodology.

## Installation

You must first ~~sharpen your knife~~ install `graph-tool` by following the [installation instructions](https://graph-tool.skewed.de/installation.html) according to your system.

Then:

```sh
python3 -m pip install sashimi-domains
```

Or to use the latest code, with more features but less stable:

```sh
python3 -m pip install git+https://gitlab.com/solstag/sashimi.git
```

### Dependencies

With the exception of graph-tool, dependencies will be automatically installed by `pip`.

This project builds mainly on the following others:
- [graph-tool](graph-tool.skewed.de/) (for stochastic block model inference)
- [pandas](https://pandas.pydata.org/) (for tabular data manipulation)
- [spacy](https://spacy.io/) (for tokenization)
- [gensim](https://radimrehurek.com/gensim/) (for n-gram detection)
- [bokeh](https://bokeh.org/) (for building interactive maps and plots)
- [lxml](https://lxml.de/) (for building HTML documents)

## Basic usage

### Instantiating and loading data
Let's instantiate a corpus class with default storage dir `./auto_sashimi`.

```python
from sashimi import GraphModels

corpus = GraphModels()
```

Load a CSV file as a `pandas.DataFrame`, then integrate it to the corpus with a name.

```python
import pandas as pd

df = pd.read_csv("example_corpus.csv")
corpus.load_data(df, name='example')

# Let's check out what was loaded
print(corpus.data)
```

For later autoloading, let's store a copy of the data under the project's storage dir.

```python
corpus.store_data()
```

### Preparing your corpus

#### Textual data
For typical usage with a textual corpus, we start by declaring the columns from which to source text, then set up how text shall be processed.

```python
corpus.text_sources = ["title", "body"]

process_sources_args = dict(ngrams=3, language="en", stop_words=True)
```

The `language` is any [valid language code for `spacy`](https://spacy.io/usage/models#languages) and determines the tokenizer and the list of stop-words to be removed. If no language is provided, we'll use the English tokenizer but won't perform stop-words removal (unless `stop_words` is explicitly set to `True`). Our English tokenizer is slightly improved from `spacy`'s original, in order to get ["hot-dog"] out of "hot-dog" (when spacy would split that), ["this", "that"] out of "this/that", and ["citation"] out of "citation[2,3]".

#### Token data
You may also directly use tokens that you have processed yourself, or from token data such as keywords, categories, or anything really, in addition or in place of textual data. Just declare the columns from which to source tokens.

```python
corpus.token_sources = ["keywords", "categories"]
```

Token sources are expected to be in the format of a sequence of strings or of a sequence of sequences of strings (`Sequence[str] | Sequence[Sequence[str]]]`). If your data doesn't fit that description, you must adjust it before processing.

#### Processing the data
Once token and/or text sources are set up, you can process them by calling:

```python
corpus.process_sources(**process_sources_args)
```

#### Setting up auxiliary columns
Some other columns in your data may contain useful information to visualize or analyse the corpus. It's time to set them up.

```python
# `col_title` is required!
# If your corpus doesn't have a column with titles, be creative
corpus.col_title = "titles"

# Dates and URLs are optional
corpus.col_time = "years"
corpus.col_urls = "urls" # can be a list of columns, if your documents have multiple urls
```

At this point you're ready to work with your corpus.


### Working with your corpus

Load a domain-topic model and check out the resulting blockstate and state data.

```python
corpus.load_domain_topic_model()

print(corpus.state)
print(corpus.dblocks) # domain blocks
print(corpus.tblocks) # topic blocks
```

Create an interactive hierarchical map. The output is a self-contained html+css+js+data document, and its path will be returned by the method.

```python
corpus.domain_map()
```

Create network representations for the first and second domain levels with first topic levels. These will be output as pdf, svg and graphml documents.

```python
corpus.domain_network(doc_level=1, ter_level=1)
corpus.domain_network(doc_level=2, ter_level=1)

# The default representation shows edges from a domain to its "specific" topics, with the previous call being equivalent to the following
corpus.domain_network(doc_level=2, ter_level=1, edges="specific")

# For domains of level higher than 1, setting `edges="common"` will show edges towards topics that are common to all of its subdomains
corpus.domain_network(doc_level=2, ter_level=1, edges="common")

# Create domain-topic tables for all domains at level 3
if 3 in corpus.dblocks:
    corpus.subxblocks_tables(xbtype="doc", xlevel=3, xb=None, ybtype="ter")
```

#### Recording and reloading your corpus' state
To store the current choices of corpus and model:

```python
corpus.register_config("my_config.json")
```

To reload them in a future session:

```python
from sashimi import GraphModels
corpus = GraphModels('my_config.json')
corpus.load_domain_topic_model()  # will load the previously calculated model
corpus.load_domain_topic_model(load=False)  # will fit a new model
print(corpus.list_blockstates())

# Load a domain-chained model over the column "metadata_A"
corpus.set_chain(prop='metadata_A')
corpus.load_domain_chained_model()
print(corpus.list_chainedbstates())

# Create interactive instruments for the chained dimension
corpus.domain_map(chained=True)
corpus.domain_network(doc_level=1, ter_level=None, ext_level=1)
corpus.domain_network(doc_level=1, ter_level=1, ext_level=1)
# The `edges` parameter applies to both topics and the chained dimension
corpus.domain_network(doc_level=2, ter_level=None, ext_level=1, edges="specific")
corpus.domain_network(doc_level=2, ter_level=1, ext_level=1, edges="common")
if 3 in corpus.dblocks:
    corpus.subxblocks_tables(xbtype="doc", xlevel=3, xb=None, ybtype="ext")
```

<img alt="Domain-chained map" src="https://docs.cortext.net/wp-content/uploads/Screenshot-from-2023-06-28-11-02-16.png" width="50%"><img alt="Domain-topic-chained network" src="https://docs.cortext.net/wp-content/uploads/Screenshot-from-2022-11-19-17-45-15.png" width="50%">

## Advanced usage

- **Help:** the domain map document provides a "Help" tab explaining how to navigate and read it, which is also useful to understand the other interfaces.

- **Filters:** Create filtered visualizations to show only a selected group of domains in maps and networks, with `Corpus.set_selection()` or with the `xb_selection` parameter to `domain_network()`.

- **Filtered chaining**: With `Corpus.set_selection()`, perform selective chaining, whereby a domain-chained model is fit by considering only a selected group of domains, instead of the entire corpus, in order to understand the local associations of metadata dimensions.

- **Complex chaining**: Calls to `Corpus.set_chain()` may pass in the `matcher` parameter a path to a `.json` file containing a dictionary. Nodes of the chained dimension will then correspond to the keys in the dictionary, and links will be established by searching for a key's value, as a regular expression, in the column passed as the first parameter.

- **Better network map**: An entirely new network map function with many improvements and new features is under development and can be tried from `from sashimi.blocks.better_network_map import network_map`. It will eventually replace the current network map.

<div align="center">
<img
  alt="Domain-authors network"
  src="https://docs.cortext.net/wp-content/uploads/Image-2.png"
  width="62%">
</div>

Development
-----------

This module provides four main classes:

`class Corpus`

Methods to load and preprocess corpora, plus some descriptive statistics.

`class GraphModels` (user-facing class, inherits Corpus and Blocks)

Methods to fit Stochastic Block Models to corpora through either document-term or document-metadata graphs, yielding domain-topic and domain-chained models, respectively.

`class Blocks`

Methods to build interactive domain maps, networks, tables, and other interfaces.

`class VectorModels` [currently deprecated]

Methods to fit word embedding models to corpora and produce reports, statistics and visualizations.
